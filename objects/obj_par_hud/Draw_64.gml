/// @description Вставьте описание здесь
// Вы можете записать свой код в этом редакторе
text_x = 256;
text_y = 50;
panel_x = 190;
panel_y = 860;
shadow_x = 311;
shadow_y = 907;
border = 60;
shadow_size = 81;

if (obj_game.hud_on == true) {
layer_set_visible("HUD",true)
layer_set_visible("HUD_shadow",true)

// Set the font
draw_set_font(fnt_points);

// Set the font's horizontal alignment
draw_set_halign(fa_left);

// Set the font's vertical alignment
draw_set_valign(fa_middle);

switch (room) {
case rm_loc1:
draw_text(text_x,text_y,obj_game.points1_left);
draw_text(text_x+100,text_y," | ");
draw_text(text_x+200,text_y,obj_game.points1_all);
break;
case rm_loc2:
draw_text(text_x,text_y,obj_game.points2_left);
draw_text(text_x+100,text_y," | ");
draw_text(text_x+200,text_y,obj_game.points2_all);
break;
case rm_loc3:
draw_text(text_x,text_y,obj_game.points3_left);
draw_text(text_x+100,text_y," | ");
draw_text(text_x+200,text_y,obj_game.points3_all);
break;

}
if !instance_exists(obj_ui_ho_panel){
instance_create_layer(panel_x,panel_y, "HUD", obj_ui_ho_panel);
}
switch (room) {
	case rm_loc1:
	if !instance_exists(obj_shadow_10) {
	instance_create_layer(shadow_x,shadow_y,"HUD_shadow",obj_shadow_10)
	}
		if !instance_exists(obj_shadow_11) {
instance_create_layer(shadow_x+border+shadow_size,shadow_y,"HUD_shadow",obj_shadow_11)
}
	if !instance_exists(obj_shadow_12) {
instance_create_layer(shadow_x+border*2+shadow_size*2,shadow_y,"HUD_shadow",obj_shadow_12)
}
	if !instance_exists(obj_shadow_13) {
instance_create_layer(shadow_x+border*3+shadow_size*3,shadow_y,"HUD_shadow",obj_shadow_13)
}
		if !instance_exists(obj_shadow_14) {
instance_create_layer(shadow_x+border*4+shadow_size*4,shadow_y,"HUD_shadow",obj_shadow_14)
}
	break;
	
case rm_loc2:
if !instance_exists(obj_shadow_20) {
instance_create_layer(shadow_x,shadow_y,"HUD_shadow",obj_shadow_20)
	}
if !instance_exists(obj_shadow_21) {
instance_create_layer(shadow_x+border+shadow_size,shadow_y,"HUD_shadow",obj_shadow_21)
}
if !instance_exists(obj_shadow_22) {
instance_create_layer(shadow_x+border*2+shadow_size*2,shadow_y,"HUD_shadow",obj_shadow_22)
}
if !instance_exists(obj_shadow_23) {
instance_create_layer(shadow_x+border*3+shadow_size*3,shadow_y,"HUD_shadow",obj_shadow_23)
}
	if !instance_exists(obj_shadow_24) {
instance_create_layer(shadow_x+border*4+shadow_size*4,shadow_y,"HUD_shadow",obj_shadow_24)
}
if !instance_exists(obj_shadow_25) {
instance_create_layer(shadow_x+border*5+shadow_size*5,shadow_y,"HUD_shadow",obj_shadow_25)
}
	if !instance_exists(obj_shadow_26) {
instance_create_layer(shadow_x+border*6+shadow_size*6,shadow_y,"HUD_shadow",obj_shadow_26)
}
	break;
	
	case rm_loc3:
		if !instance_exists(obj_shadow_30) {
instance_create_layer(shadow_x,shadow_y,"HUD_shadow",obj_shadow_30)
}

			if !instance_exists(obj_shadow_31) {
instance_create_layer(shadow_x+border+shadow_size,shadow_y,"HUD_shadow",obj_shadow_31)
}

	if !instance_exists(obj_shadow_32) {
instance_create_layer(shadow_x+border*2+shadow_size*2,shadow_y,"HUD_shadow",obj_shadow_32)
}

	if !instance_exists(obj_shadow_33) {
instance_create_layer(shadow_x+border*3+shadow_size*3,shadow_y,"HUD_shadow",obj_shadow_33)
}

		if !instance_exists(obj_shadow_34) {
instance_create_layer(shadow_x+border*4+shadow_size*4,shadow_y,"HUD_shadow",obj_shadow_34)
}

	if !instance_exists(obj_shadow_35) {
instance_create_layer(shadow_x+border*5+shadow_size*5,shadow_y,"HUD_shadow",obj_shadow_35)
}

		if !instance_exists(obj_shadow_36) {
instance_create_layer(shadow_x+border*6+shadow_size*6,shadow_y,"HUD_shadow",obj_shadow_36)
}

	if !instance_exists(obj_shadow_37) {
instance_create_layer(shadow_x+border*7+shadow_size*7,shadow_y,"HUD_shadow",obj_shadow_37)
}

	if !instance_exists(obj_shadow_38) {
instance_create_layer(shadow_x+border*8+shadow_size*8,shadow_y,"HUD_shadow",obj_shadow_38)
}

		if !instance_exists(obj_shadow_39) {
instance_create_layer(shadow_x+border*9+shadow_size*9,shadow_y,"HUD_shadow",obj_shadow_39)
}

	if !instance_exists(obj_shadow_310) {
instance_create_layer(shadow_x+border*10+shadow_size*10,shadow_y,"HUD_shadow",obj_shadow_310)
}

	
	break;
}}


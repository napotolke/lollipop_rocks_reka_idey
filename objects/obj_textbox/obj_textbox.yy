{
  "resourceType": "GMObject",
  "resourceVersion": "1.0",
  "name": "obj_textbox",
  "eventList": [],
  "managed": true,
  "overriddenProperties": [],
  "parent": {
    "name": "dialogues",
    "path": "folders/Objects/dialogues.yy",
  },
  "parentObjectId": null,
  "persistent": false,
  "physicsAngularDamping": 0.1,
  "physicsDensity": 0.5,
  "physicsFriction": 0.2,
  "physicsGroup": 1,
  "physicsKinematic": false,
  "physicsLinearDamping": 0.1,
  "physicsObject": false,
  "physicsRestitution": 0.1,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsShapePoints": [],
  "physicsStartAwake": true,
  "properties": [],
  "solid": false,
  "spriteId": {
    "name": "spr_ui_dialogue_panel",
    "path": "sprites/spr_ui_dialogue_panel/spr_ui_dialogue_panel.yy",
  },
  "spriteMaskId": null,
  "visible": true,
}
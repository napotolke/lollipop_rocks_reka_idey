/// @description Вставьте описание здесь
// Вы можете записать свой код в этом редакторе

//rm_comic0 - comic0 exist, when end create obj_dlg_0
if (comic0_end = 1) && !instance_exists(obj_dlg_0) && !(dlg0_end = 1) {
		instance_create_layer(0,0, "Instances", obj_dlg0_background);
	instance_create_layer(x,y, "Instances", obj_dlg_0);
	
}
if (dlg0_end = 1) {
room_goto(rm_loc1);
			
}
//rm_comic0 exit

//rm_loc1 - obj_dlg1 exists, when end create obj_game with hud

if (dlg1_end = 1) && !instance_exists(obj_game) {
	instance_create_layer(x,y, "Instances",obj_game);
			
}
//if points1_left = 0 create obj obj_loc1_after
if instance_exists(obj_game) && (obj_game.points1_left == 0) && !(instance_exists(obj_loc1_after)) {
	instance_create_depth(0,0,-16000, obj_loc1_after);
	alarm[0] = 90;

}
//rm_loc1 exit

//rm_comic1 - after comic we creating dlg2

if (comic1_end = 1) && !instance_exists(obj_dlg_2) && !(dlg2_end = 1) && !(instance_exists(obj_dlg2_background)) {
		instance_create_layer(0,0, "Instances", obj_dlg2_background);
	instance_create_layer(x,y, "Instances", obj_dlg_2);
	
}

//when dlg2 ends go to rm_comic2
if (dlg2_end = 1) {
	room_goto(rm_comic2);
	}

//rm_comic1 exit


//rm_comic2 when comic ends go to loc2
if (comic2_end = 1) {
	room_goto(rm_loc2);
	
}
//rm_comic2 exit

//rm_loc2 - dlg3 exists, after it ends we create obj_game

	
if (dlg3_end = 1) && !instance_exists(obj_game) {
	instance_create_layer(x,y, "Instances",obj_game);
	
	}

//when points2 left = 0, create obj_loc2_after than go to rm_comic3 with alarm

if instance_exists(obj_game) && (obj_game.points2_left == 0) && !(instance_exists(obj_loc2_after)) {
	instance_create_depth(0,0,-16000, obj_loc2_after);
	alarm[1] = 90;

}

//rm_loc2 exit

//rm_comic3 - dlg4 exists in room, no comics just background for dlg4

if (dlg4_end = 1) {
room_goto(rm_loc3);
			
}

//rm_comic3 exit

//rm_loc3 starts gameplay, obj game already exists

//when points3 left = 0, create obj_loc3_after than go to rm_comic5 with alarm

if instance_exists(obj_game) && (obj_game.points3_left == 0) && !(instance_exists(obj_loc3_after)) {
	instance_create_depth(0,0,-16000, obj_loc3_after);
	alarm[2] = 90;

}

//rm_loc3 exit

//rm_comic5 - first obj_comic5 starts after it ends obj_dlg5 goes

//creating dlg5 and background
if (comic5_end = 1) && !instance_exists(obj_dlg_5) && !(dlg5_end = 1)  {
		instance_create_layer(0,0, "Instances", obj_dlg5_background);
	instance_create_layer(x,y, "Instances", obj_dlg_5);
	
}

//after dlg5 ends we go to rm_comic6
if (dlg5_end = 1) {
		room_goto(rm_comic6);
			
}

//after comic6 ens go to authors
if (comic6_end = 1) {
		room_goto(rm_main_menu);
			
}
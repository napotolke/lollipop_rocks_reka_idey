/// @description Вставьте описание здесь
// Вы можете записать свой код в этом редакторе
/// @description 

if(showing_dialog == true) {
	
	var height = 32;
	var border = 5;
	var padding = 16;	
	var text_box_x = 258;
	var text_box_y = 737;
	
	height = string_height(current_dialog.message);
	
	if(sprite_get_height(current_dialog.sprite) > height) {
		height = sprite_get_height(current_dialog.sprite);
	}
	
	height += padding * 2;
	text_x = sprite_get_width(current_dialog.sprite) + (padding * 2);
	
	draw_set_alpha(alpha);
	if !instance_exists(obj_textbox) {
	instance_create_depth(text_box_x,text_box_y,-10000,obj_textbox);
	}
	// drawing black recktangle size of a screen
	//draw_set_color(c_black);
	//draw_rectangle(0, 0, display_get_gui_width(), height, false);
	
	// drawing white border
	//draw_set_color(c_white);
	//draw_rectangle(border, border, display_get_gui_width() - border, height - border, false);
	
	// drawing small black rectangle insede white b
	//draw_set_color(c_black);
	//draw_rectangle((border * 2), (border * 2), display_get_gui_width() - (border * 2), height - (border * 2), false);
	
	if(current_dialog.sprite != -1) {
		draw_sprite(current_dialog.sprite, 0, text_box_x + border * 3,text_box_y + border * 3);
	}
	
	draw_set_font(fnt_dlg);
	draw_set_color(c_white);
	
	// set text position interval between lines, space from right
	draw_text_ext(text_box_x + 250, text_box_y + 70, current_dialog.message, 50, display_get_gui_width() - 670);
	
	alpha = lerp(alpha, 1, 0.06);
}

draw_set_alpha(1);
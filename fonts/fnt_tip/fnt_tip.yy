{
  "resourceType": "GMFont",
  "resourceVersion": "1.0",
  "name": "fnt_tip",
  "AntiAlias": 1,
  "applyKerning": 0,
  "ascender": 25,
  "ascenderOffset": 2,
  "bold": false,
  "canGenerateBitmap": true,
  "charset": 0,
  "first": 0,
  "fontName": "Verdana",
  "glyphOperations": 0,
  "glyphs": {
    "32": {"character":32,"h":34,"offset":0,"shift":10,"w":10,"x":2,"y":2,},
    "33": {"character":33,"h":34,"offset":2,"shift":11,"w":8,"x":324,"y":146,},
    "34": {"character":34,"h":34,"offset":4,"shift":12,"w":10,"x":334,"y":146,},
    "35": {"character":35,"h":34,"offset":2,"shift":22,"w":19,"x":346,"y":146,},
    "36": {"character":36,"h":34,"offset":0,"shift":17,"w":17,"x":367,"y":146,},
    "37": {"character":37,"h":34,"offset":2,"shift":29,"w":26,"x":386,"y":146,},
    "38": {"character":38,"h":34,"offset":1,"shift":20,"w":18,"x":414,"y":146,},
    "39": {"character":39,"h":34,"offset":3,"shift":7,"w":6,"x":434,"y":146,},
    "40": {"character":40,"h":34,"offset":1,"shift":12,"w":13,"x":442,"y":146,},
    "41": {"character":41,"h":34,"offset":-2,"shift":12,"w":13,"x":457,"y":146,},
    "42": {"character":42,"h":34,"offset":3,"shift":17,"w":14,"x":472,"y":146,},
    "43": {"character":43,"h":34,"offset":3,"shift":22,"w":17,"x":488,"y":146,},
    "44": {"character":44,"h":34,"offset":-1,"shift":10,"w":8,"x":2,"y":182,},
    "45": {"character":45,"h":34,"offset":2,"shift":12,"w":9,"x":12,"y":182,},
    "46": {"character":46,"h":34,"offset":1,"shift":10,"w":5,"x":23,"y":182,},
    "47": {"character":47,"h":34,"offset":-3,"shift":12,"w":17,"x":30,"y":182,},
    "48": {"character":48,"h":34,"offset":1,"shift":17,"w":16,"x":49,"y":182,},
    "49": {"character":49,"h":34,"offset":2,"shift":17,"w":12,"x":67,"y":182,},
    "50": {"character":50,"h":34,"offset":0,"shift":17,"w":16,"x":81,"y":182,},
    "51": {"character":51,"h":34,"offset":0,"shift":17,"w":17,"x":99,"y":182,},
    "52": {"character":52,"h":34,"offset":0,"shift":17,"w":16,"x":306,"y":146,},
    "53": {"character":53,"h":34,"offset":1,"shift":17,"w":17,"x":287,"y":146,},
    "54": {"character":54,"h":34,"offset":1,"shift":17,"w":16,"x":269,"y":146,},
    "55": {"character":55,"h":34,"offset":1,"shift":17,"w":18,"x":35,"y":146,},
    "56": {"character":56,"h":34,"offset":1,"shift":17,"w":16,"x":404,"y":110,},
    "57": {"character":57,"h":34,"offset":2,"shift":17,"w":15,"x":422,"y":110,},
    "58": {"character":58,"h":34,"offset":2,"shift":12,"w":8,"x":439,"y":110,},
    "59": {"character":59,"h":34,"offset":0,"shift":12,"w":10,"x":449,"y":110,},
    "60": {"character":60,"h":34,"offset":3,"shift":22,"w":17,"x":461,"y":110,},
    "61": {"character":61,"h":34,"offset":3,"shift":22,"w":17,"x":480,"y":110,},
    "62": {"character":62,"h":34,"offset":4,"shift":22,"w":16,"x":2,"y":146,},
    "63": {"character":63,"h":34,"offset":3,"shift":15,"w":13,"x":20,"y":146,},
    "64": {"character":64,"h":34,"offset":2,"shift":27,"w":24,"x":55,"y":146,},
    "65": {"character":65,"h":34,"offset":-2,"shift":18,"w":19,"x":248,"y":146,},
    "66": {"character":66,"h":34,"offset":0,"shift":19,"w":18,"x":81,"y":146,},
    "67": {"character":67,"h":34,"offset":1,"shift":19,"w":19,"x":101,"y":146,},
    "68": {"character":68,"h":34,"offset":0,"shift":21,"w":21,"x":122,"y":146,},
    "69": {"character":69,"h":34,"offset":0,"shift":17,"w":19,"x":145,"y":146,},
    "70": {"character":70,"h":34,"offset":0,"shift":16,"w":19,"x":166,"y":146,},
    "71": {"character":71,"h":34,"offset":1,"shift":21,"w":21,"x":187,"y":146,},
    "72": {"character":72,"h":34,"offset":0,"shift":20,"w":21,"x":210,"y":146,},
    "73": {"character":73,"h":34,"offset":0,"shift":11,"w":13,"x":233,"y":146,},
    "74": {"character":74,"h":34,"offset":-2,"shift":12,"w":15,"x":118,"y":182,},
    "75": {"character":75,"h":34,"offset":0,"shift":19,"w":21,"x":155,"y":182,},
    "76": {"character":76,"h":34,"offset":0,"shift":15,"w":14,"x":444,"y":218,},
    "77": {"character":77,"h":34,"offset":0,"shift":23,"w":23,"x":178,"y":182,},
    "78": {"character":78,"h":34,"offset":0,"shift":20,"w":21,"x":71,"y":218,},
    "79": {"character":79,"h":34,"offset":1,"shift":21,"w":20,"x":94,"y":218,},
    "80": {"character":80,"h":34,"offset":0,"shift":16,"w":18,"x":116,"y":218,},
    "81": {"character":81,"h":34,"offset":1,"shift":21,"w":20,"x":136,"y":218,},
    "82": {"character":82,"h":34,"offset":0,"shift":19,"w":18,"x":158,"y":218,},
    "83": {"character":83,"h":34,"offset":0,"shift":18,"w":19,"x":178,"y":218,},
    "84": {"character":84,"h":34,"offset":2,"shift":17,"w":18,"x":199,"y":218,},
    "85": {"character":85,"h":34,"offset":2,"shift":20,"w":19,"x":219,"y":218,},
    "86": {"character":86,"h":34,"offset":2,"shift":18,"w":19,"x":240,"y":218,},
    "87": {"character":87,"h":34,"offset":3,"shift":27,"w":26,"x":261,"y":218,},
    "88": {"character":88,"h":34,"offset":-1,"shift":19,"w":22,"x":289,"y":218,},
    "89": {"character":89,"h":34,"offset":2,"shift":17,"w":18,"x":313,"y":218,},
    "90": {"character":90,"h":34,"offset":0,"shift":19,"w":20,"x":333,"y":218,},
    "91": {"character":91,"h":34,"offset":0,"shift":12,"w":14,"x":355,"y":218,},
    "92": {"character":92,"h":34,"offset":3,"shift":12,"w":8,"x":371,"y":218,},
    "93": {"character":93,"h":34,"offset":-1,"shift":12,"w":14,"x":381,"y":218,},
    "94": {"character":94,"h":34,"offset":3,"shift":22,"w":18,"x":397,"y":218,},
    "95": {"character":95,"h":34,"offset":-3,"shift":17,"w":18,"x":417,"y":218,},
    "96": {"character":96,"h":34,"offset":7,"shift":17,"w":5,"x":437,"y":218,},
    "97": {"character":97,"h":34,"offset":0,"shift":16,"w":15,"x":54,"y":218,},
    "98": {"character":98,"h":34,"offset":0,"shift":17,"w":16,"x":36,"y":218,},
    "99": {"character":99,"h":34,"offset":1,"shift":14,"w":14,"x":20,"y":218,},
    "100": {"character":100,"h":34,"offset":1,"shift":17,"w":17,"x":328,"y":182,},
    "101": {"character":101,"h":34,"offset":1,"shift":16,"w":15,"x":203,"y":182,},
    "102": {"character":102,"h":34,"offset":0,"shift":10,"w":14,"x":220,"y":182,},
    "103": {"character":103,"h":34,"offset":0,"shift":17,"w":17,"x":236,"y":182,},
    "104": {"character":104,"h":34,"offset":0,"shift":17,"w":16,"x":255,"y":182,},
    "105": {"character":105,"h":34,"offset":0,"shift":7,"w":8,"x":273,"y":182,},
    "106": {"character":106,"h":34,"offset":-4,"shift":9,"w":14,"x":283,"y":182,},
    "107": {"character":107,"h":34,"offset":0,"shift":16,"w":17,"x":299,"y":182,},
    "108": {"character":108,"h":34,"offset":0,"shift":7,"w":8,"x":318,"y":182,},
    "109": {"character":109,"h":34,"offset":0,"shift":26,"w":25,"x":347,"y":182,},
    "110": {"character":110,"h":34,"offset":0,"shift":17,"w":16,"x":2,"y":218,},
    "111": {"character":111,"h":34,"offset":1,"shift":16,"w":15,"x":374,"y":182,},
    "112": {"character":112,"h":34,"offset":-1,"shift":17,"w":17,"x":391,"y":182,},
    "113": {"character":113,"h":34,"offset":1,"shift":17,"w":16,"x":410,"y":182,},
    "114": {"character":114,"h":34,"offset":0,"shift":12,"w":14,"x":428,"y":182,},
    "115": {"character":115,"h":34,"offset":0,"shift":14,"w":14,"x":444,"y":182,},
    "116": {"character":116,"h":34,"offset":1,"shift":11,"w":11,"x":460,"y":182,},
    "117": {"character":117,"h":34,"offset":1,"shift":17,"w":16,"x":473,"y":182,},
    "118": {"character":118,"h":34,"offset":2,"shift":16,"w":15,"x":491,"y":182,},
    "119": {"character":119,"h":34,"offset":2,"shift":22,"w":21,"x":381,"y":110,},
    "120": {"character":120,"h":34,"offset":-1,"shift":16,"w":18,"x":135,"y":182,},
    "121": {"character":121,"h":34,"offset":0,"shift":16,"w":17,"x":362,"y":110,},
    "122": {"character":122,"h":34,"offset":-1,"shift":14,"w":16,"x":437,"y":38,},
    "123": {"character":123,"h":34,"offset":2,"shift":17,"w":16,"x":23,"y":38,},
    "124": {"character":124,"h":34,"offset":5,"shift":12,"w":3,"x":41,"y":38,},
    "125": {"character":125,"h":34,"offset":-1,"shift":17,"w":17,"x":46,"y":38,},
    "126": {"character":126,"h":34,"offset":3,"shift":22,"w":18,"x":65,"y":38,},
    "1025": {"character":1025,"h":34,"offset":0,"shift":17,"w":19,"x":85,"y":38,},
    "1026": {"character":1026,"h":34,"offset":2,"shift":21,"w":19,"x":106,"y":38,},
    "1027": {"character":1027,"h":34,"offset":0,"shift":15,"w":19,"x":127,"y":38,},
    "1028": {"character":1028,"h":34,"offset":1,"shift":19,"w":19,"x":148,"y":38,},
    "1029": {"character":1029,"h":34,"offset":0,"shift":18,"w":19,"x":169,"y":38,},
    "1030": {"character":1030,"h":34,"offset":0,"shift":11,"w":13,"x":190,"y":38,},
    "1031": {"character":1031,"h":34,"offset":0,"shift":11,"w":14,"x":205,"y":38,},
    "1032": {"character":1032,"h":34,"offset":-2,"shift":12,"w":15,"x":221,"y":38,},
    "1033": {"character":1033,"h":34,"offset":-2,"shift":30,"w":31,"x":238,"y":38,},
    "1034": {"character":1034,"h":34,"offset":0,"shift":30,"w":29,"x":271,"y":38,},
    "1035": {"character":1035,"h":34,"offset":2,"shift":22,"w":19,"x":302,"y":38,},
    "1036": {"character":1036,"h":34,"offset":0,"shift":19,"w":21,"x":323,"y":38,},
    "1037": {"character":1037,"h":34,"offset":0,"shift":20,"w":21,"x":346,"y":38,},
    "1038": {"character":1038,"h":34,"offset":0,"shift":17,"w":20,"x":369,"y":38,},
    "1039": {"character":1039,"h":34,"offset":0,"shift":20,"w":21,"x":391,"y":38,},
    "1040": {"character":1040,"h":34,"offset":-2,"shift":18,"w":19,"x":2,"y":38,},
    "1041": {"character":1041,"h":34,"offset":0,"shift":19,"w":19,"x":470,"y":2,},
    "1042": {"character":1042,"h":34,"offset":0,"shift":19,"w":18,"x":450,"y":2,},
    "1043": {"character":1043,"h":34,"offset":0,"shift":15,"w":19,"x":206,"y":2,},
    "1044": {"character":1044,"h":34,"offset":-3,"shift":20,"w":23,"x":14,"y":2,},
    "1045": {"character":1045,"h":34,"offset":0,"shift":17,"w":19,"x":39,"y":2,},
    "1046": {"character":1046,"h":34,"offset":-2,"shift":26,"w":30,"x":60,"y":2,},
    "1047": {"character":1047,"h":34,"offset":-1,"shift":17,"w":18,"x":92,"y":2,},
    "1048": {"character":1048,"h":34,"offset":0,"shift":20,"w":21,"x":112,"y":2,},
    "1049": {"character":1049,"h":34,"offset":0,"shift":20,"w":21,"x":135,"y":2,},
    "1050": {"character":1050,"h":34,"offset":0,"shift":19,"w":21,"x":158,"y":2,},
    "1051": {"character":1051,"h":34,"offset":-2,"shift":20,"w":23,"x":181,"y":2,},
    "1052": {"character":1052,"h":34,"offset":0,"shift":23,"w":23,"x":227,"y":2,},
    "1053": {"character":1053,"h":34,"offset":0,"shift":20,"w":21,"x":427,"y":2,},
    "1054": {"character":1054,"h":34,"offset":1,"shift":21,"w":20,"x":252,"y":2,},
    "1055": {"character":1055,"h":34,"offset":0,"shift":20,"w":21,"x":274,"y":2,},
    "1056": {"character":1056,"h":34,"offset":0,"shift":16,"w":18,"x":297,"y":2,},
    "1057": {"character":1057,"h":34,"offset":1,"shift":19,"w":19,"x":317,"y":2,},
    "1058": {"character":1058,"h":34,"offset":2,"shift":17,"w":18,"x":338,"y":2,},
    "1059": {"character":1059,"h":34,"offset":0,"shift":17,"w":20,"x":358,"y":2,},
    "1060": {"character":1060,"h":34,"offset":1,"shift":22,"w":21,"x":380,"y":2,},
    "1061": {"character":1061,"h":34,"offset":-1,"shift":19,"w":22,"x":403,"y":2,},
    "1062": {"character":1062,"h":34,"offset":0,"shift":21,"w":21,"x":414,"y":38,},
    "1063": {"character":1063,"h":34,"offset":2,"shift":19,"w":18,"x":455,"y":38,},
    "1064": {"character":1064,"h":34,"offset":0,"shift":28,"w":29,"x":305,"y":110,},
    "1065": {"character":1065,"h":34,"offset":0,"shift":28,"w":29,"x":475,"y":38,},
    "1066": {"character":1066,"h":34,"offset":2,"shift":21,"w":19,"x":420,"y":74,},
    "1067": {"character":1067,"h":34,"offset":0,"shift":25,"w":26,"x":441,"y":74,},
    "1068": {"character":1068,"h":34,"offset":0,"shift":18,"w":18,"x":469,"y":74,},
    "1069": {"character":1069,"h":34,"offset":0,"shift":19,"w":19,"x":489,"y":74,},
    "1070": {"character":1070,"h":34,"offset":0,"shift":28,"w":28,"x":2,"y":110,},
    "1071": {"character":1071,"h":34,"offset":-2,"shift":19,"w":22,"x":32,"y":110,},
    "1072": {"character":1072,"h":34,"offset":0,"shift":16,"w":15,"x":56,"y":110,},
    "1073": {"character":1073,"h":34,"offset":1,"shift":17,"w":16,"x":73,"y":110,},
    "1074": {"character":1074,"h":34,"offset":0,"shift":16,"w":15,"x":91,"y":110,},
    "1075": {"character":1075,"h":34,"offset":0,"shift":13,"w":15,"x":108,"y":110,},
    "1076": {"character":1076,"h":34,"offset":-3,"shift":17,"w":19,"x":125,"y":110,},
    "1077": {"character":1077,"h":34,"offset":1,"shift":16,"w":15,"x":146,"y":110,},
    "1078": {"character":1078,"h":34,"offset":-1,"shift":22,"w":24,"x":163,"y":110,},
    "1079": {"character":1079,"h":34,"offset":-1,"shift":14,"w":15,"x":189,"y":110,},
    "1080": {"character":1080,"h":34,"offset":0,"shift":17,"w":17,"x":206,"y":110,},
    "1081": {"character":1081,"h":34,"offset":0,"shift":17,"w":17,"x":225,"y":110,},
    "1082": {"character":1082,"h":34,"offset":0,"shift":16,"w":17,"x":244,"y":110,},
    "1083": {"character":1083,"h":34,"offset":-2,"shift":17,"w":19,"x":263,"y":110,},
    "1084": {"character":1084,"h":34,"offset":0,"shift":19,"w":19,"x":284,"y":110,},
    "1085": {"character":1085,"h":34,"offset":0,"shift":17,"w":17,"x":401,"y":74,},
    "1086": {"character":1086,"h":34,"offset":1,"shift":16,"w":15,"x":384,"y":74,},
    "1087": {"character":1087,"h":34,"offset":0,"shift":17,"w":17,"x":365,"y":74,},
    "1088": {"character":1088,"h":34,"offset":-1,"shift":17,"w":17,"x":156,"y":74,},
    "1089": {"character":1089,"h":34,"offset":1,"shift":14,"w":14,"x":2,"y":74,},
    "1090": {"character":1090,"h":34,"offset":1,"shift":13,"w":14,"x":18,"y":74,},
    "1091": {"character":1091,"h":34,"offset":0,"shift":16,"w":17,"x":34,"y":74,},
    "1092": {"character":1092,"h":34,"offset":1,"shift":23,"w":21,"x":53,"y":74,},
    "1093": {"character":1093,"h":34,"offset":-1,"shift":16,"w":18,"x":76,"y":74,},
    "1094": {"character":1094,"h":34,"offset":0,"shift":17,"w":17,"x":96,"y":74,},
    "1095": {"character":1095,"h":34,"offset":2,"shift":16,"w":14,"x":115,"y":74,},
    "1096": {"character":1096,"h":34,"offset":0,"shift":24,"w":23,"x":131,"y":74,},
    "1097": {"character":1097,"h":34,"offset":0,"shift":24,"w":23,"x":175,"y":74,},
    "1098": {"character":1098,"h":34,"offset":1,"shift":17,"w":15,"x":348,"y":74,},
    "1099": {"character":1099,"h":34,"offset":0,"shift":21,"w":21,"x":200,"y":74,},
    "1100": {"character":1100,"h":34,"offset":0,"shift":15,"w":14,"x":223,"y":74,},
    "1101": {"character":1101,"h":34,"offset":-1,"shift":15,"w":15,"x":239,"y":74,},
    "1102": {"character":1102,"h":34,"offset":0,"shift":23,"w":22,"x":256,"y":74,},
    "1103": {"character":1103,"h":34,"offset":-1,"shift":16,"w":17,"x":280,"y":74,},
    "1105": {"character":1105,"h":34,"offset":1,"shift":16,"w":15,"x":299,"y":74,},
    "8210": {"character":8210,"h":34,"offset":2,"shift":17,"w":14,"x":316,"y":74,},
    "8211": {"character":8211,"h":34,"offset":2,"shift":17,"w":14,"x":332,"y":74,},
    "8212": {"character":8212,"h":34,"offset":2,"shift":27,"w":24,"x":336,"y":110,},
    "9647": {"character":9647,"h":34,"offset":5,"shift":26,"w":16,"x":460,"y":218,},
  },
  "hinting": 0,
  "includeTTF": false,
  "interpreter": 0,
  "italic": false,
  "kerningPairs": [],
  "last": 0,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/Fonts.yy",
  },
  "pointRounding": 0,
  "ranges": [
    {"lower":32,"upper":127,},
    {"lower":1025,"upper":1105,},
    {"lower":8210,"upper":8212,},
    {"lower":9647,"upper":9647,},
  ],
  "regenerateBitmap": false,
  "sampleText": "",
  "size": 20.0,
  "styleName": "Italic",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "TTFName": "",
}
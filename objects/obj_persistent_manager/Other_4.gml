/// @description Вставьте описание здесь
// Вы можете записать свой код в этом редакторе
if (room == rm_loc1) || (room == rm_loc2) || (room == rm_loc3)
{
	// Stop the comic music
	audio_stop_sound(snd_comic);
	
	// Play the game music
	audio_play_sound(snd_game, 1, true);
}
else
{
	// Stop the game music
	audio_stop_sound(snd_game);
	
	// Play the comic music
	audio_play_sound(snd_comic, 1, true);
}
